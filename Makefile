.PHONY: help
help:
	@	echo  'make clean      - Remove every created directory and restart from scratch'
	@	echo  'make docs       - Install Sphinx and requirements, then build documentations'
	@	echo  'make prepare    - Easy way for make everything'
	@	echo  'make tests      - Easy way for make tests over a CI'
	@	echo  ''
	@	echo  'sudo make install-python   - Easy way to install python3 pip and venv'

.PHONY: install-python
install-python:
	@	echo ""
	@	echo "******************************* INSTALL PYTHON *********************************"
	@	apt-get update && apt install -y python3 python3-pip python3-venv portaudio19-dev

.PHONY: header
header:
	@echo "**************************** GALAXIE SASHA MAKEFILE ****************************"
	@echo "HOSTNAME	`uname -n`"
	@echo "KERNEL RELEASE `uname -r`"
	@echo "KERNEL VERSION `uname -v`"
	@echo "PROCESSOR	`uname -m`"
	@echo "********************************************************************************"

.PHONY: prepare
prepare: header
	@ direnv reload
	@	pip3 install -U pip --no-cache-dir --quiet &&\
		echo "[  OK  ] PIP3" || \
		echo "[FAILED] PIP3"
	@	pip3 install -U wheel --no-cache-dir --quiet &&\
		echo "[  OK  ] WHEEL" || \
		echo "[FAILED] WHEEL"
	@	pip3 install -U setuptools --no-cache-dir --quiet &&\
		echo "[  OK  ] SETUPTOOLS" || \
		echo "[FAILED] SETUPTOOLS"
	@ pip3 install -e .

.PHONY: tests
tests: prepare
	@ 	echo ""
	@	echo "********************************** START TESTS *********************************"
	@	python setup.py tests

.PHONY: clean
clean: header
	@ 	echo ""
	@ 	echo "*********************************** CLEAN UP ***********************************"
	rm -rf ./venv
	rm -rf ./.direnv
	rm -rf ~/.cache/pip
	rm -rf ./.eggs
	rm -rf ./*.egg-info
	rm -rf .coverage

.PHONY: docs
docs: prepare
	@ 	echo ""
	@ 	echo "***************************** BUILD DOCUMENTATIONS *****************************"
	@		pip3 install -r $(PWD)/docs/requirements.txt --no-cache-dir --quiet && cd $(PWD)/docs && make html
