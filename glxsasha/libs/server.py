import time
import zmq
import sys
from glxsasha.libs.daemon import Daemon
from glxsasha.libs.paser import parser_server
from glxsasha.libs.brain import Brain


class Server(Daemon):
    def __init__(self, pidfile):
        Daemon.__init__(self, pidfile)
        self.__port = None
        self.__brain = None

        self.port = None
        self.brain = None
        self.running = False

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop()

    @property
    def port(self):
        """
        Store the port use for socket connexion

        :return: port number
        :rtype: int
        """
        return self.__port

    @port.setter
    def port(self, value):
        """
        Set the port value property

        :param value: port use for connexion socket
        :type value: int or None
        :raise TypeError: When port value property is not int type or None
        """
        if value is None:
            value = 5555
        if type(value) != int:
            raise TypeError("'port' porperty value must be a type int or None")
        if self.port != value:
            self.__port = value

    @property
    def brain(self):
        """
        The main brain

        :return: port number
        :rtype: glxsasha.libs.brain.Brain
        """
        return self.__brain

    @brain.setter
    def brain(self, value):
        """
        Set the brain value property

        :param value: the main brain
        :type value:  glxsasha.libs.brain.Brain or None
        :raise TypeError: When port value property is not glxsasha.libs.brain.Brain instance or None
        """
        if value is None:
            value = Brain()
        if not isinstance(value, Brain):
            raise TypeError("'brain' porperty value must be a type glxsasha.libs.brain.Brain instance or None")
        if self.brain != value:
            self.__brain = value

    def run(self):
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.REP)
        self.socket.bind("tcp://*:{port}".format(port=self.port))
        self.running = True
        sys.stdout.write('Start server\n')
        self.brain.load_brain()
        self.brain.load_session()
        while self.running:
            #  Wait for next request from client
            recive = self.socket.recv_string()
            # sys.stdout.write('Received request: {0}\n'.format(recive))

            #  Do some 'work'
            # time.sleep(1)

            response = self.brain.kernel.respond(
                recive,
                self.brain.session_name,
            )

            #  Send reply back to client
            if response:
                self.socket.send_string(response)
            else:
                self.socket.send_string("")


def main(argv=None):  # pragma: no cover
    daemon = Server('/tmp/daemon-example.pid')

    args = parser_server.parse_args(argv)
    if args.start:
        daemon.start()
    elif args.stop:
        daemon.stop()
    elif args.restart:
        daemon.restart()
    else:
        sys.stdout.write("Unknown command\n")
        parser_server.print_usage()
        sys.exit(2)
    sys.exit(0)


if __name__ == "__main__":
    main()
