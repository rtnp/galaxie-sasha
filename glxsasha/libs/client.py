import zmq
import sys

from cmd import Cmd


class Client(Cmd):
    prompt = '> '
    intro = '***************************** GLXSAHA CLIENT V0.1 ******************************\n'

    def __init__(self):
        Cmd.__init__(self)
        self.__address = None
        self.__port = None
        self.address = None
        self.port = None

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def default(self, line):
        # self.socket.send(bytes(line, 'UTF-8'))
        if line and len(line) > 0 and line != "":
            self.socket.send_string(line)
            message = self.socket.recv_string()
            sys.stdout.write("{0}\n".format(message))

        return None

    @property
    def port(self):
        """
        Store the port use for socket connexion

        :return: port number
        :rtype: int
        """
        return self.__port

    @port.setter
    def port(self, value):
        """
        Set the port value property

        :param value: port use for connexion socket
        :type value: int or None
        :raise TypeError: When port value property is not int type or None
        """
        if value is None:
            value = 5555
        if type(value) != int:
            raise TypeError("'port' porperty value must be a type int or None")
        if self.port != value:
            self.__port = value

    @property
    def address(self):
        """
        Store the address use for socket connexion

        :return: address number
        :rtype: str
        """
        return self.__address

    @address.setter
    def address(self, value):
        """
        Set the address value property

        :param value: address use for connexion socket
        :type value: str or None
        :raise TypeError: When address value property is not int type or None
        """
        if value is None:
            value = "localhost"
        if type(value) != str:
            raise TypeError("'address' porperty value must be a type int or None")
        if self.address != value:
            self.__address = value

    def do_EOF(self, line):
        sys.stdout.write('\n')
        return True

    def connect(self):
        #  Socket to talk to server
        self.context = zmq.Context()
        sys.stdout.write("Connecting to server…\n")
        self.socket = self.context.socket(zmq.REQ)
        self.socket.connect("tcp://{address}:{port}".format(
            address=self.address,
            port=self.port
        ))


def main():  # pragma: no cover
    with Client() as client:
        sys.exit(client.cmdloop())


if __name__ == "__main__":
    main()
    sys.exit(0)
