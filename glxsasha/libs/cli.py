import sys
from cmd import Cmd
from glxsasha.libs.core import clear
from glxsasha.libs.paser import WithCmdArgParser
from glxsasha.libs.paser import parser_clear


class Cli(Cmd):
    prompt = '> '
    intro = '******************************** GLXSAHA V0.1 **********************************\n'

    def __init__(self, *args, **kwargs):
        super().__init__()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return 0

    def do_EOF(self, line):
        sys.stdout.write('\n')
        return True

    def default(self, line):
        sys.stdout.write('Sasha: {0}: unknow command\n'.format(line))
        return None

    # Commands
    @WithCmdArgParser(parser_clear)
    def do_clear(self, *args, **kwargs):
        clear()
        return None

    def help_clear(self):
        parser_clear.print_help()


def main():  # pragma: no cover
    if len(sys.argv) > 1:
        sys.exit(Cli().onecmd(' '.join(sys.argv[1:])))
    else:
        sys.exit(Cli().cmdloop())
