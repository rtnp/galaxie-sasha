from argparse import ArgumentParser


class WithCmdArgParser:
    def __init__(self, parser):
        self.parser = parser

    def __call__(self, func):
        if not self.parser:  # pragma: no cover
            self.parser = func(None, None, None, True)

        def wrapped_function(*args, **kwargs):
            try:
                return func(*args, parsed=self.parser.parse_args(args[1].split()), **kwargs)
            except SystemExit:  # pragma: no cover
                return

        return wrapped_function


class _ArgError(BaseException):
    pass


parser_clear = ArgumentParser(prog="clear", description="clear the terminal screen")

parser_server = ArgumentParser(prog="glxsasha-server", description="Server it start AI Sasha")
parser_server.add_argument(
    "--start",
    action="store_true",
    default=False,
    help="Start server",
)
parser_server.add_argument(
    "--stop",
    action="store_true",
    default=False,
    help="Stop server",
)
parser_server.add_argument(
    "--restart",
    action="store_true",
    default=False,
    help="Restart server",
)
parser_server.add_argument(
    "-p",
    "--port",
    type=int,
    help="Port use by the server",
)
