import aiml
import os
import marshal
from xdg import xdg_data_home


class Brain(object):
    def __init__(
        self,
        data_home=None,
        brain_name=None,
        brain_filename=None,
        kernel=None,
        session_name=None,
        session_filename=None,
        modules_directory=None,
    ):

        self.__data_home = None
        self.__brain_name = None
        self.__brain_filename = None
        self.__kernel = None
        self.__session_name = None
        self.__session_filename = None
        self.__modules_directory = None

        self.data_home = data_home
        self.brain_name = brain_name
        self.brain_filename = brain_filename
        self.kernel = kernel
        self.session_name = session_name
        self.session_filename = session_filename
        self.modules_directory = modules_directory

    def __enter__(self):
        self.load_brain()
        self.load_session()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.save_session()
        self.save_brain()

    @property
    def brain_file(self):
        """
        Internal function for return the real path of the brain file

        :return: real path of the brain file
        :rtype: str
        """
        return os.path.join(
            self.data_home,
            self.brain_filename,
        )

    @property
    def brain_filename(self):
        return self.__brain_filename

    @brain_filename.setter
    def brain_filename(self, value=None):
        if value is None:
            value = "{0}.brain".format(self.brain_name)
        if type(value) != str:
            raise TypeError("'brain_filename' property value must be a str or None")
        if self.brain_filename != value:
            self.__brain_filename = value

    @property
    def brain_name(self):
        return self.__brain_name

    @brain_name.setter
    def brain_name(self, value=None):
        if value is None:
            value = "sasha"
        if type(value) != str:
            raise TypeError("'brain_name' property value must be a str or None")
        if self.brain_name != value:
            self.__brain_name = value

    @property
    def data_home(self):
        """
        Store data_home  property value, it correspond to directory where session files will be stored.
        """
        return self.__data_home

    @data_home.setter
    def data_home(self, value):
        if value is None:
            value = os.path.join(xdg_data_home(), "glxsasha")
        if type(value) != str:
            raise TypeError("'data_home' property value must be a str or None")
        if self.data_home != value:
            self.__data_home = value

    def check_data_home(self):
        if not os.path.exists(self.data_home):
            os.makedirs(self.data_home, mode=0o755, exist_ok=False)

    @property
    def kernel(self):
        """
        Store the aiml.Kernel object

        :return: The kernel object wrapped by the Brain
        :rtype: aiml.Kernel
        """
        return self.__kernel

    @kernel.setter
    def kernel(self, value):
        """
        Store the aiml.Kernel object

        :param value: The kernel object wrapped by the Brain
        :type value: aiml.Kernel or None
        """

        if value is None:
            value = aiml.Kernel()
        if not isinstance(value, aiml.Kernel):
            raise TypeError("'kernel' property value must be a aiml.Kernel instance or None")
        if self.kernel != value:
            self.__kernel = value

    def load_aiml(self):
        # Use the 'learn' method to load the contents
        # of an AIML file into the Kernel.
        for aiml_file in self.modules_files:
            self.kernel.learn(aiml_file)

    def load_brain(self):
        """
        read dictionary and create brain in file
        """
        if os.path.isfile(self.brain_file):
            self.kernel.bootstrap(brainFile=self.brain_file)

        else:
            self.load_aiml()

            self.kernel.setPredicate("master", self.brain_file)

            self.save_brain()

        # In every case it name the bot
        self.kernel.setBotPredicate("name", self.brain_file)

    def load_session(self):
        if os.path.isfile(self.session_file):
            print("load session")
            with open(self.session_file, "rb") as session_file:
                for predicate, value in marshal.load(session_file).items():
                    self.kernel.setPredicate(
                        predicate,
                        value,
                        self.session_name,
                    )

    @property
    def modules_directory(self):
        """
        Store the location of directories where aiml bots files are stored

        :return: The location of modules directory
        :rtype: str
        """
        return self.__modules_directory

    @modules_directory.setter
    def modules_directory(self, value):
        """
        Store the location of directories where aiml bots files are stored

        :param value: The path where the bot files are stored
        :type value: str or None
        """
        if value is None:
            value = os.path.realpath(
                os.path.join(
                    os.path.dirname(os.path.realpath(__file__)), "..", "modules"
                )
            )
        if type(value) != str:
            raise TypeError("'modules_directory' property value must be a str type or None")
        if self.modules_directory != value:
            self.__modules_directory = value

    @property
    def modules_files(self):
        return [os.path.join(root, name)
                for root, dirs, files in os.walk(self.modules_directory)
                for name in files
                if name.endswith((".aiml", ".AIML", "Aiml"))]

    def save_brain(self):
        self.check_data_home()
        self.kernel.saveBrain(self.brain_file)

    def save_session(self):
        self.check_data_home()
        with open(self.session_file, "wb") as session_file:
            marshal.dump(
                self.kernel.getSessionData(self.session_name),
                session_file,
            )

    @property
    def session_file(self):
        """
        Internal function for return the real path of the brain file

        :return: real path of the brain file
        :rtype: str
        """
        return os.path.join(
            self.data_home,
            self.session_filename,
        )

    @property
    def session_filename(self):
        return self.__session_filename

    @session_filename.setter
    def session_filename(self, value=None):
        if value is None:
            value = "{0}.session".format(self.session_name)
        if type(value) != str:
            raise TypeError("'session_filename' property value must be a str or None")
        if self.session_filename != value:
            self.__session_filename = value

    @property
    def session_name(self):
        """
        Store the session name

        :return: The session name
        :rtype: str
        """
        return self.__session_name

    @session_name.setter
    def session_name(self, value):
        """
        Store the session name

        :param value: The session name
        :type value: str
        """

        if value is None:
            value = "default"
        if type(value) != str:
            raise TypeError("'session_name' property value must be a str type or None")
        if self.session_name != value:
            self.__session_name = value
# Use the 'respond' method to compute the response
# to a user's input string.  respond() returns
# the interpreter's response, which in this case
# we ignore.
# k.respond("load aiml b")

# Loop forever, reading user input from the command
# line and printing responses.
# while True: print k.respond(raw_input("> "))
