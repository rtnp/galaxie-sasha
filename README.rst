Galaxie Sasha
*************

Introduction
============
Sasha est une **Intelligence Artificielle Low Tech** dédiée à la gestion d'une maison, qu'elle considère 
comme son quartier général (similairement à ce que l'on voit dans certains films ou dessins animés).

Sasha se pilote principalement à la voix, et ceci afin d'être utilisé en cas d'handicap moteur.

Sasha est un majordome numérique capable d'installer, de configurer et de maintenir les machines constituant
son quartier général. On parle ici des serveurs, imprimantes, modem, firewall...

Il organise les moyens de télécommunication à intérieur de sa base et peut s'interconnecter avec d'autres bases.

Réference
=========
La documentation se trouve dans le répertoire `docs/sources`

Developpement
=============

Générer la documentation
------------------------

.. code:: shell

	make docs

