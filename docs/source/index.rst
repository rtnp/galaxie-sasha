.. title:: Galaxie Sasha

.. image:: images/sasha_logo.png
  :alt: Logo

Galaxie Sasha Documentation
===========================

* **Actual Homepage**: https://gitlab.com/rtnp/galaxie-sasha
* **Documentation**: https://galaxie-sasha.readthedocs.io

The Project
-----------

The Mission
-----------

Usage
-----

Features
--------

Contribute
----------
* **Issue Tracker**: https://gitlab.com/rtnp/galaxie-sasha/issues
* **Source Code**: https://gitlab.com/rtnp/galaxie-sasha

Our collaboration model is the Collective Code Construction Contract (C4): https://rfc.zeromq.org/spec:22/C4/


License
-------
WTFPL – Do What the Fuck You Want to Public License


See the LICENCE_

.. _LICENCE: https://gitlab.com/rtnp/galaxie-sasha/blob/master/LICENSE.rst

All contributions to the project source code ("patches") SHALL use the same license as the project.

Indices and tables
------------------
* :ref:`genindex`
* :ref:`search`
