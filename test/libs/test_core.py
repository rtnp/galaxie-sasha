import unittest
import sys
from glxsasha.libs.core import clear
from io import StringIO
from unittest.mock import patch


class TestCore(unittest.TestCase):
    def test_clear(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            sys.stdout.write("Hello.42")
            self.assertEqual(fake_out.getvalue(), "Hello.42")
            clear()
            self.assertEqual(fake_out.getvalue(), "Hello.42\x1b[2J\x1b[H")
