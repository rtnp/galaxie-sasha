import unittest
from glxsasha.libs.server import Server
from glxsasha.libs.brain import Brain


class TestServer(unittest.TestCase):
    def setUp(self):
        self.server = Server('/tmp/daemon-example.pid')

    def test_init(self):
        with Server('/tmp/daemon-example.pid') as server:
            server.running = False

    def test_port(self):
        self.assertEqual(self.server.port, 5555)
        self.server.port = 42
        self.assertEqual(self.server.port, 42)
        self.server.port = None
        self.assertEqual(self.server.port, 5555)
        self.assertRaises(TypeError, setattr, self.server, 'port', "Hello42")

    def test_brain(self):
        self.assertTrue(isinstance(self.server.brain, Brain))
        brain = Brain()
        self.server.brain = brain

        self.assertEqual(self.server.brain, brain)
        self.server.brain = None
        self.assertNotEqual(self.server.brain, brain)
        self.assertTrue(isinstance(self.server.brain, Brain))
        self.assertRaises(TypeError, setattr, self.server, 'brain', "Hello42")
