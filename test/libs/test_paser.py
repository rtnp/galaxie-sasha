import unittest
from glxsasha.libs.paser import WithCmdArgParser
from argparse import ArgumentParser

class TestParser(unittest.TestCase):
    def setUp(self):
        pass

    def test_withcmdargparser(self):
        self.assertTrue(isinstance(WithCmdArgParser(None), WithCmdArgParser))
        self.assertTrue(isinstance(WithCmdArgParser(parser=ArgumentParser()), WithCmdArgParser))

