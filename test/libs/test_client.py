import unittest
from glxsasha.libs.client import Client


class TestClient(unittest.TestCase):
    def setUp(self):
        self.client = Client()

    def test_port(self):
        self.assertEqual(self.client.port, 5555)
        self.client.port = 42
        self.assertEqual(self.client.port, 42)
        self.client.port = None
        self.assertEqual(self.client.port, 5555)
        self.assertRaises(TypeError, setattr, self.client, 'port', "Hello42")

    def test_address(self):
        self.assertEqual(self.client.address, "localhost")
        self.client.address = "hello.42"
        self.assertEqual(self.client.address, "hello.42")
        self.client.address = None
        self.assertEqual(self.client.address, "localhost")
        self.assertRaises(TypeError, setattr, self.client, 'address', 42)
