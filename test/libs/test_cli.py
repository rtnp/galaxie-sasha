import unittest
import sys
from glxsasha.libs.cli import Cli
from io import StringIO
from unittest.mock import patch


class TestCli(unittest.TestCase):
    def setUp(self):
        with Cli() as cli:
            self.cli = cli

    def test_clear(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            sys.stdout.write("Hello.42")
            self.assertEqual(fake_out.getvalue(), "Hello.42")
            self.cli.onecmd("clear")
            self.assertEqual(fake_out.getvalue(), "Hello.42\x1b[2J\x1b[H")

    def test_help_clear(self):
        expected = """usage: clear [-h]

clear the terminal screen

optional arguments:
  -h, --help  show this help message and exit
"""
        with patch("sys.stdout", new=StringIO()) as fake_out:
            self.cli.help_clear()
            self.assertEqual(fake_out.getvalue(), expected)

    def test_default(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            self.cli.default("42")
            self.assertEqual(fake_out.getvalue(), "Sasha: 42: unknow command\n")

    def test_EOF(self):
        with patch("sys.stdout", new=StringIO()) as fake_out:
            self.assertTrue(self.cli.do_EOF("42"))
            self.assertEqual(fake_out.getvalue(), "\n")

