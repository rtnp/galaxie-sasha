import unittest
import os
import aiml
import tempfile
from glxsasha.libs.brain import Brain
from xdg import xdg_data_home


class TestBrain(unittest.TestCase):
    def setUp(self):
        self.brain = Brain()

    def test_data_home(self):
        self.assertEqual(self.brain.data_home, os.path.join(xdg_data_home(), "glxsasha"))

        self.assertRaises(TypeError, setattr, self.brain, "data_home", 42)

    def test_brain_name(self):
        self.assertEqual(self.brain.brain_name, "sasha")
        self.brain.brain_name = "Hello.42"
        self.assertEqual(self.brain.brain_name, "Hello.42")
        self.brain.brain_name = None
        self.assertEqual(self.brain.brain_name, "sasha")
        self.assertRaises(TypeError, setattr, self.brain, "brain_name", 42)

    def test_brain_filename(self):
        self.assertEqual(self.brain.brain_filename, "{0}.brain".format(self.brain.brain_name))
        self.brain.brain_filename = "Hello.42"
        self.assertEqual(self.brain.brain_filename, "Hello.42")
        self.brain.brain_filename = None
        self.assertEqual(self.brain.brain_filename, "{0}.brain".format(self.brain.brain_name))
        self.assertRaises(TypeError, setattr, self.brain, "brain_filename", 42)

    def test_brain_file(self):
        self.assertEqual(
            self.brain.brain_file,
            os.path.join(self.brain.data_home, self.brain.brain_filename)
        )

    def test_kernel(self):
        self.assertTrue(isinstance(self.brain.kernel, aiml.Kernel))

        kernel = aiml.Kernel()
        self.brain.kernel = kernel
        self.assertEqual(self.brain.kernel, kernel)

        self.brain.kernel = None
        self.assertNotEqual(self.brain.kernel, kernel)
        self.assertTrue(isinstance(self.brain.kernel, aiml.Kernel))

        self.assertRaises(TypeError, setattr, self.brain, 'kernel', 42)

    def test_session_name(self):
        self.assertEqual(self.brain.session_name, "default")
        self.brain.session_name = "Hello.42"
        self.assertEqual(self.brain.session_name, "Hello.42")
        self.brain.session_name = None
        self.assertEqual(self.brain.session_name, "default")
        self.assertRaises(TypeError, setattr, self.brain, "session_name", 42)

    def test_session_filename(self):
        self.assertEqual(self.brain.session_filename, "{0}.session".format(self.brain.session_name))
        self.brain.session_filename = "Hello.42"
        self.assertEqual(self.brain.session_filename, "Hello.42")
        self.brain.session_filename = None
        self.assertEqual(self.brain.session_filename, "{0}.session".format(self.brain.session_name))
        self.assertRaises(TypeError, setattr, self.brain, "session_filename", 42)

    def test_session_file(self):
        self.assertEqual(
            self.brain.session_file,
            os.path.join(self.brain.data_home, self.brain.session_filename)
        )

    def test_check_data_home(self):
        tmpdirname = tempfile.TemporaryDirectory()
        self.brain.data_home = tmpdirname.name
        tmpdirname.cleanup()
        self.assertFalse(os.path.exists(self.brain.data_home))
        self.brain.check_data_home()
        self.assertTrue(os.path.exists(self.brain.data_home))
        os.rmdir(self.brain.data_home)

    def test_save_brain(self):
        tmpdirname = tempfile.TemporaryDirectory()
        self.brain.data_home = tmpdirname.name
        tmpdirname.cleanup()
        self.assertFalse(os.path.exists(self.brain.data_home))
        self.assertFalse(os.path.exists(self.brain.brain_file))
        self.brain.save_brain()
        self.assertTrue(os.path.exists(self.brain.data_home))
        self.assertTrue(os.path.exists(self.brain.brain_file))
        os.remove(self.brain.brain_file)
        os.rmdir(self.brain.data_home)
        self.assertFalse(os.path.exists(self.brain.data_home))
        self.assertFalse(os.path.exists(self.brain.brain_file))

    def test_load_brain(self):
        tmpdirname = tempfile.TemporaryDirectory()
        self.brain.data_home = tmpdirname.name
        tmpdirname.cleanup()
        self.assertFalse(os.path.exists(self.brain.data_home))
        self.assertFalse(os.path.exists(self.brain.brain_file))
        self.brain.save_brain()
        self.assertTrue(os.path.exists(self.brain.data_home))
        self.assertTrue(os.path.exists(self.brain.brain_file))
        self.brain.load_brain()
        os.remove(self.brain.brain_file)
        os.rmdir(self.brain.data_home)
        self.assertFalse(os.path.exists(self.brain.data_home))
        self.assertFalse(os.path.exists(self.brain.brain_file))

    def test_load_save(self):
        tmpdirname = tempfile.TemporaryDirectory()
        self.brain.data_home = tmpdirname.name
        tmpdirname.cleanup()
        self.assertFalse(os.path.exists(self.brain.data_home))
        self.assertFalse(os.path.exists(self.brain.brain_file))
        self.assertFalse(os.path.exists(self.brain.session_file))
        self.brain.save_brain()
        self.brain.save_session()
        self.assertTrue(os.path.exists(self.brain.data_home))
        self.assertTrue(os.path.exists(self.brain.brain_file))
        self.assertTrue(os.path.exists(self.brain.session_file))
        self.brain.load_brain()
        self.brain.load_session()
        os.remove(self.brain.brain_file)
        os.remove(self.brain.session_file)
        os.rmdir(self.brain.data_home)
        self.assertFalse(os.path.exists(self.brain.data_home))
        self.assertFalse(os.path.exists(self.brain.brain_file))
        self.assertFalse(os.path.exists(self.brain.session_file))

    def test_with_init(self):
        tmpdirname = tempfile.TemporaryDirectory()

        with Brain(data_home=tmpdirname.name) as brain:
            tmpdirname.cleanup()
            data_home = brain.data_home
            brain_file = brain.brain_file
            session_file = brain.session_file
            brain.kernel.respond(
                "Hello",
                brain.session_name
            )
            brain.save_brain()
            brain.save_session()
            brain.load_brain()
            brain.load_session()

        self.assertTrue(os.path.exists(data_home))
        self.assertTrue(os.path.exists(brain_file))
        self.assertTrue(os.path.exists(session_file))
        os.remove(brain_file)
        os.remove(session_file)
        os.rmdir(data_home)
        self.assertFalse(os.path.exists(data_home))
        self.assertFalse(os.path.exists(brain_file))
        self.assertFalse(os.path.exists(session_file))

    def test_modules_directory(self):
        self.assertEqual(self.brain.modules_directory, os.path.realpath(
            os.path.join(
                os.path.dirname(os.path.realpath(__file__)), "..", "..", "glxsasha", "modules"
            ))
        )
        self.brain.modules_directory = "Hello.42"
        self.assertEqual(self.brain.modules_directory, "Hello.42")
        self.brain.modules_directory = None
        self.assertEqual(self.brain.modules_directory, os.path.realpath(
            os.path.join(
                os.path.dirname(os.path.realpath(__file__)), "..", "..", "glxsasha", "modules"
            ))
        )
        self.assertRaises(TypeError, setattr, self.brain, "modules_directory", 42)
